import { Time } from '@angular/common';
import { Transferencia } from './../models/transferencia.model';
import { TransferenciaService } from './../services/transferencia.service';
import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Output } from "@angular/core";
import { Router } from '@angular/router';

@Component({
    selector: 'app-nova-transferencia',
    templateUrl: './nova-transferencia.component.html',
    styleUrls: ['./nova-transferencia.component.scss']
})
export class NovaTransferenciaComponent{

  @Output() aoTransferir = new EventEmitter<any>();

  leitura: number;
  estacao: string;
  observador: string;
  dialeitura: Date;
  horaleitura: Time;

  constructor(private service: TransferenciaService, private router: Router) {}

  transferir(){
    console.log('Solicitada nova transferência');
    const valorEmitir: Transferencia = {
      leitura: this.leitura, observador: this.observador, estacao: this.estacao, dialeitura: this.dialeitura, horaleitura: this.horaleitura
    };
    this.service.adicionar(valorEmitir).subscribe(resultado => {
        console.log(resultado);
        this.limparCampos();
        this.router.navigateByUrl('extrato');
    },
    (error) => console.error(error)
    );
  }

  limparCampos(){
    this.leitura = 0;
  }
}
