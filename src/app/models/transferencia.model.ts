import { Time } from "@angular/common";

export interface Transferencia {
  observador: string;
  leitura: number;
  estacao: number | string;
  dialeitura: Date;
  horaleitura: Time;
}
export interface Observador {
  observador: string;
  nome: string;
  cpf: string;
  email: string;
  org: string;
}
