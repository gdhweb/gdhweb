import { ObservadorService } from './../services/observador.service';
import { Component, Input, OnInit } from '@angular/core';
import { Observador } from '../models/transferencia.model';

@Component({
  selector: 'app-observador',
  templateUrl: './observador.component.html',
  styleUrls: ['./observador.component.scss']
})
export class ObservadorComponent implements OnInit {

  observadores: any[];

  constructor(private service: ObservadorService) { }

  ngOnInit(): void {
      this.service.todas().subscribe((observadores: Observador[]) => {
        console.table(observadores);
        this.observadores = observadores;
      })
  }

}
