import { NovaTransferenciaComponent } from '../nova-transferencia/nova-transferencia.component';
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { Routes } from '@angular/router';
import { ExtratoComponent } from "../extrato/extrato.component";
import { ObservadorComponent } from "../observador/observador.component";

export const routes: Routes = [
  {path: '', redirectTo: 'observador', pathMatch: 'full'},
  {path: 'extrato', component: ExtratoComponent},
  {path: 'observador', component: ObservadorComponent},
  {path: 'nova-transferencia', component: NovaTransferenciaComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule{}
