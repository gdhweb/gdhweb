import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Observador } from '../models/transferencia.model';

@Injectable({
  providedIn: 'root'
})

export class ObservadorService {
  private listaObservador: any[];
  private url = 'http://localhost:3001/observadores'

constructor( private httpClient: HttpClient) {
  this.listaObservador = [];
}

get observadores (){
  return this.listaObservador;
}

todas(): Observable<Observador[]> {
  return this.httpClient.get<Observador[]>(this.url);
}

adicionar(observador: Observador): Observable<Observador>{
  this.hidratar(observador);
  return this.httpClient.post<Observador>(this.url, observador)
}

private hidratar(observador: any) {
  observador.data = new Date();
}
}
